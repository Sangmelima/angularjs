
function Moncontoller($scope , $http , monService1) {
	$scope.title ="liste";
	$http({
		  method: 'get',
		  url: 'http://localhost:8081/AppRest/ws/personnes'
		}).success(function(data, status, headers, config) {
		  console.log('Success: ' + data);
		  $scope.users = data;
		});
	
	$scope.connection = function() {
		monService1.connect($scope.selecteduser);
	};
	$scope.$on('etatconnection',function(event , data){
		$scope.personne = data.personne;
		$scope.isconnected = data.isconnected;
	});
}

function Calculcontroller($scope ,calculservice){
	
	$scope.calculer = function(){
		var result = 0;
		if($scope.operator == '+'){
			result = calculservice.add($scope.a,$scope.b);
		}else if($scope.operator == '-'){
			result = calculservice.Moins($scope.a,$scope.b);
		}else if($scope.operator == '*'){
			result = calculservice.mult($scope.a,$scope.b);
		}
		$scope.result = result;
	};
}

function Messagecontroller($scope ,$routeParams){
	
	$scope.mess = $routeParams.message;
	$scope.userName = $routeParams.userName;
	$scope.search = $routeParams.search;
	//formulaire 
	$scope.nom = "bhb";
	$scope.valider = function(){
		
		if($scope.monformulaire.nom.$valid){
			alert('le  nom est valide');
		}else {
			alert('le  nom est invalide');
		}
	};

	$scope.title = "validation formulaire"
}


var MonService1 = function($http , $rootScope) {
	var isconnected = false;
	this.isconnected = function(){
		return isconnected;
	}
	this.connect = function(id) {
		//juste  un test
		$http({
		  method: 'get',
		  url: 'http://localhost:8081/AppRest/ws/personne/'+id
		}).success(function(data, status, headers, config) {
			if(data.role == 'admin'){
				isconnected = true;
			}else {
				isconnected = false;
			}
			//publicaion  des  valeurs  à tous ceux  qui suivenet  cet etat  "etatconnection"
			$rootScope.$broadcast('etatconnection',{personne:data,isconnected:isconnected});
		  console.log('Success: ' + data);
		}).error(function(error) {

		});

	}
	
};

//service calculatrice
var calculservice = function() {
	
	this.add = function(a, b){
		return parseInt(a)+parseInt(b);
	};
	this.mult = function(a, b){
		return parseInt(a)* parseInt(b);
	};
	this.Moins = function(a, b){
		return parseInt(a) - parseInt(b);
	};
	
}


var  module = angular.module("MonApp" , ["ngRoute"]);
module.service('monService1',MonService1);
module.service('calculservice',calculservice);
module.controller("Moncontoller",Moncontoller);
module.controller("Calculcontroller",Calculcontroller);
module.controller("Messagecontroller",Messagecontroller);


module.config(function($routeProvider){
	
	$routeProvider.when("/testConnexion",{
		templateUrl:"views/testconnexions.jsp",
		controller : "Moncontoller"
	}).when("/calculatrice",{
		templateUrl:"views/calculatrice.jsp",
		controller : "Calculcontroller"
	}).when("/messageParam/:message?/user/:userName?",{
		templateUrl:"views/message_validation.jsp",
		controller : "Messagecontroller"
	}).otherwise({redirectTo:"/"});
	
});

